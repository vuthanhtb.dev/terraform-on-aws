# Terraform Block
terraform {
  required_version = ">= 1.0" # which means any version equal & above 1.0 like 1.15, 1.16 etc and < 2.xx
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  } 
}  
# Provider Block
provider "aws" {
  region  = "ap-southeast-1"
  profile = "default"
}

/*
Note-1:  AWS Credentials Profile (profile = "default") configured on your local desktop terminal  
  $HOME/.aws/credentials

Note-2: Execute Terraform Commands
1. terraform init
2. terraform validate
3. terraform plan
4. terraform apply -auto-approve
5. terraform destroy -auto-approve

Note-3: Clean-Up Files after destroy
  rm -rf .terraform*
  rm -rf terraform.tfstate*
*/
