# configure aws credentials
# login aws
# Go to Services -> IAM -> Users -> "Your-Admin-User" -> Security Credentials -> Create Access Key

# 1. Ensure you have default-vpc in that respective region
# 2. Ensure AMI you are provisioning exists in that region if not update AMI ID
# 3. Verify your AWS Credentials in $HOME/.aws/credentials
# Run: terraform init -> terraform validate -> terraform plan -> terraform apply -auto-approve -> terraform destroy -auto-approve

# terraform setting blocks
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      # version = "~> 4.33" # Optional but recommended in production
    }
  }
}

# provider block
provider "aws" {
  profile = "default" # aws credentials profile configured on your local desktop terminal $HOME/.aws/credentials
  region  = "ap-southeast-1"
}

# resouce block
resource "aws_instance" "ec2demo" {
  ami           = "ami-0f62d9254ca98e1aa" # amazon linux in ap-southeast-1, update as per your region
  instance_type = "t2.micro"
}
